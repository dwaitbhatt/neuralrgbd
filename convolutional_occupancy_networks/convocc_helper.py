import torch
# from src import config
from src.checkpoints import CheckpointIO

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
def get_convocc_model(config_path='configs/default.yaml'):    
    # Make a convocc config file and update path here
    cfg = config.load_config(config_path)
    convocc_model = config.get_model(cfg, device=device)

    print("Loaded ConvOcc model")

    checkpoint_io = CheckpointIO(model=convocc_model)
    checkpoint_io.load(cfg['test']['model_file'])

    return convocc_model

