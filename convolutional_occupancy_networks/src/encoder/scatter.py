import numpy as np
import torch

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

def scatter_max2(src, index, dim_size=None):
    if dim_size is None:
        dim_size = torch.max(index) + 1
    out_shape = (src.size(1), dim_size)
    # print("out_shape", out_shape, src.shape, index.shape)
    out = torch.full(out_shape,  float('-inf')).unsqueeze(0).to(device)
    # print("real out", out.shape)
    src_shape = src.shape
    for i in range(src_shape[1]):
        for j in range(src_shape[2]):
            # print(out[0][i][index[0,0][j]])
            out[0][i][index[0,0][j]] = torch.max(out[0][i][index[0,0][j]],src[0][i][j])
    return out#.unsqueeze(0)

def scatter_max(src, index, dim_size=None):
    if dim_size is None:
        dim_size = torch.max(index) + 1
    out_shape = (dim_size, src.size(1))
    out = torch.full(out_shape, float('-inf'), dtype=src.dtype).to(device)
    argmax = torch.zeros(out_shape, dtype=torch.int32)
    print("src", src.size(), src.size(1))

    for i in range(src.size(0)):
        for j in range(src.size(1)):
            for k in range(src.size(2)):
                idx = index[i, 0, k].item()
                if src[i, j, k] > out[idx, j]:
                    out[idx, j] = src[i, j, k]
                    argmax[idx, j] = i

    return out#, argmax

def scatter_max1(src, index, dim_size=None):
    if dim_size is None:
        dim_size = torch.max(index) + 1

    out_shape = (dim_size, src.size(1))
    out = torch.full(out_shape, float('-inf'), dtype=src.dtype, device=src.device)
    argmax = torch.zeros(out_shape, dtype=torch.int32, device=src.device)

    index_expanded = index.unsqueeze(1).expand(-1, src.size(1), -1)
    out.scatter_max_(0, index_expanded, out=out)
    argmax.scatter_(0, index_expanded, src.new_tensor(range(src.size(0))).unsqueeze(1))

    return out, argmax

# def scatter_max(src, index, dim_size=None):
#     if dim_size is None:
#         print("ind", index)
#         dim_size = torch.max(index).item() + 1
#         print(dim_size)
#     out = torch.full((dim_size, src.shape[1]), float('-inf'), dtype=src.dtype)
#     argmax = np.zeros((dim_size, src.shape[1]), dtype=np.int32)

#     for i in range(src.shape[0]):
#         for j in range(src.shape[1]):
#             for k in range(src.shape[2]):
#                 idx = index[i, j, k]
#                 if src[i, j, k] > out[idx, j]:
#                     out[idx, j] = src[i, j, k]
#                     argmax[idx, j] = i

#     return out, argmax

def scatter_mean(src, index, c=None, dim_size=None):
    if dim_size is None:
        dim_size = np.max(index) + 1
    out = np.zeros((dim_size, src.shape[1]), dtype=src.dtype)
    counts = np.zeros(dim_size, dtype=np.int32)

    for i in range(src.shape[0]):
        for j in range(src.shape[1]):
            out[index[i, j]] += src[i, j]
            counts[index[i, j]] += 1

    counts[counts == 0] = 1  # Avoid division by zero
    out /= counts.reshape((-1, 1))

    return out