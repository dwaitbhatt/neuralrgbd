expname = morning_apartment_convoccdummy
basedir = ./logs
datadir = ./data/morning_apartment
dataset_type = scannet
trainskip = 10

N_iters = 4000
N_rand = 1024
N_samples = 256
N_importance = 16
chunk = 4096  # 1024 * 16
frame_features = 0
optimize_poses = True
use_deformation_field = False
share_coarse_fine = True
multires = 8
convocc_dims = 64

rgb_weight = 0.1
depth_weight = 0.0
fs_weight = 10.0
trunc_weight = 6000.0
trunc = 0.05

rgb_loss_type = l2
sdf_loss_type = l2

mode = sdf
use_viewdirs = True
raw_noise_std = 0.0

translation = [-0.22, -0.43, 0.0]
sc_factor = 0.5
near = 0.0
far = 2.0

factor = 1
render_factor = 1

i_img = 500
i_mesh = 500
i_weights = 200
